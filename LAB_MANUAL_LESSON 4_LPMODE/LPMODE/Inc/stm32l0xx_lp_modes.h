#ifndef __STM32L0xx_LP_MODES_H
#define __STM32L0xx_LP_MODES_H
#include "main.h"

#if !defined (SLEEP_MODE) && !defined (STOP_MODE) && !defined (STANDBY_MODE)\
 && !defined (STANDBY_RTC_MODE) && !defined (STANDBY_RTC_BKPSRAM_MODE)
//#define SLEEP_MODE      
//#define STOP_MODE 
#define STANDBY_MODE
#endif

#if !defined (SLEEP_MODE) && !defined (STOP_MODE) && !defined (STANDBY_MODE)\
 && !defined (STANDBY_RTC_MODE) && !defined (STANDBY_RTC_BKPSRAM_MODE)
 #error "Please select first the target STM32L0xx Low Power mode to be measured (in stm32l0xx_lp_modes.h file)"
#endif

typedef enum 
{  
  WAKEUP_0 = 0,
  WAKEUP_1 = 1,
	WAKEUP_2 = 2
} WakeUpPin_TypeDef; 

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void SleepMode_Measure(void);
void StopMode_Measure(void);
void StandbyMode_Measure(WakeUpPin_TypeDef WakeUpPin);


#endif 

