#include "main.h"

static void SYSCLKConfig_STOP(void);
void Configuration_Wakepin(WakeUpPin_TypeDef WakeUpPin);
void SleepMode_Measure(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();

    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Pin = GPIO_PIN_All;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    __HAL_RCC_GPIOA_CLK_DISABLE();
    __HAL_RCC_GPIOB_CLK_DISABLE();
    __HAL_RCC_GPIOC_CLK_DISABLE();
    HAL_Delay(2000);
    MX_KEY_Init();
    //MX_GPIO_Init();
    HAL_SuspendTick();
    HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
    HAL_ResumeTick();
    MX_LED_Init();
}

/**
  * @brief  This function configures the system to enter Stop mode with RTC
  *         clocked by LSE or LSI  for current consumption measurement purpose.
  *         STOP Mode with RTC clocked by LSE/LSI
  *         =====================================
  *           - RTC Clocked by LSE or LSI
  *           - Regulator in LP mode
  *           - HSI, HSE OFF and LSI OFF if not used as RTC Clock source
  *           - No IWDG
  *           - FLASH in deep power down mode
  *           - Automatic Wakeup using RTC clocked by LSE/LSI (~20s)
  * @param  None
  * @retval None
  */
void StopMode_Measure(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    //   __HAL_FLASH_SLEEP_POWERDOWN_ENABLE();//关闭FLASH

    /* LP RUN Mode && LP SLEEP MODE && STOP MODE && Standby MODE可以实现功能 */
    HAL_PWREx_EnableUltraLowPower();//VREFTINT is off in low power mode
    HAL_PWREx_EnableFastWakeUp();
    __HAL_RCC_WAKEUPSTOP_CLK_CONFIG(RCC_StopWakeUpClock_HSI);


    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();

    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Pin = GPIO_PIN_All;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);


    __HAL_RCC_GPIOA_CLK_DISABLE();
    __HAL_RCC_GPIOB_CLK_DISABLE();
    __HAL_RCC_GPIOC_CLK_DISABLE();

    MX_KEY_Init();

    HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);






    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
    SYSCLKConfig_STOP();
    MX_LED_Init();
}

void StandbyMode_Measure(WakeUpPin_TypeDef WakeUpPin)
{
    if(__HAL_PWR_GET_FLAG(PWR_FLAG_SB) != RESET)//如果系统在Stanbdy唤醒，清除标志
    {
        __HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);
    }
		
		Configuration_Wakepin(WAKEUP_0);
		
    __HAL_RCC_PWR_CLK_ENABLE();//使能电源管理时钟
		
    HAL_PWREx_EnableUltraLowPower();//使能超低功耗
		
    HAL_PWREx_EnableFastWakeUp();//使能快速唤醒

    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);//清除唤醒标志，准备进入低功耗模式
    switch(WakeUpPin)
    {
    case WAKEUP_0:
        HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN1);//使能Wake UP PIN
        break;
    case WAKEUP_1:
        HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN2);//使能Wake UP PIN
        break;
    case WAKEUP_2:
        break;
    }

    HAL_PWR_EnterSTANDBYMode();//进入低功耗模式

}

static void SYSCLKConfig_STOP(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;
    uint32_t pFLatency = 0;

    HAL_RCC_GetOscConfig(&RCC_OscInitStruct);

    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.HSICalibrationValue = 0x10;
    if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }

    HAL_RCC_GetClockConfig(&RCC_ClkInitStruct, &pFLatency);
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, pFLatency) != HAL_OK)
    {
        Error_Handler();
    }
}


void Configuration_Wakepin(WakeUpPin_TypeDef WakeUpPin)
{

    GPIO_InitTypeDef GPIO_InitStruct = {0};
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();

    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

    switch(WakeUpPin)
    {
    case WAKEUP_0:
        GPIO_InitStruct.Pin = GPIO_PIN_0;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
        HAL_NVIC_SetPriority((IRQn_Type)(EXTI0_1_IRQn), 0x01, 0x00);
        HAL_NVIC_EnableIRQ((IRQn_Type)(EXTI0_1_IRQn));
        break;
    case WAKEUP_1:
        GPIO_InitStruct.Pin = GPIO_PIN_13;
        HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
        HAL_NVIC_SetPriority((IRQn_Type)(EXTI4_15_IRQn), 0x01, 0x00);
        HAL_NVIC_EnableIRQ((IRQn_Type)(EXTI4_15_IRQn));
        break;
    case WAKEUP_2:

        break;

    }
}
