/**
  ******************************************************************************
  * File Name          : LPTIM.c
  * Description        : This file provides code for the configuration
  *                      of the LPTIM instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "lptim.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

LPTIM_HandleTypeDef hlptim1;

/* LPTIM1 init function */
void MX_LPTIM1_Init(void)
{

    hlptim1.Instance = LPTIM1;//选择LPTIM
    hlptim1.Init.Clock.Source = LPTIM_CLOCKSOURCE_APBCLOCK_LPOSC;//选择时钟源
    hlptim1.Init.Clock.Prescaler = LPTIM_PRESCALER_DIV128;//设置分频系数
    hlptim1.Init.Trigger.Source = LPTIM_TRIGSOURCE_SOFTWARE;//设置触发源
    hlptim1.Init.OutputPolarity = LPTIM_OUTPUTPOLARITY_HIGH;//指定的输出电平
    hlptim1.Init.UpdateMode = LPTIM_UPDATE_IMMEDIATE;//更新模式：立即更新或者结束之后更新
    hlptim1.Init.CounterSource = LPTIM_COUNTERSOURCE_INTERNAL;//计数器源：内部或者外部
    if (HAL_LPTIM_Init(&hlptim1) != HAL_OK)
    {
        Error_Handler();
    }

}

void HAL_LPTIM_MspInit(LPTIM_HandleTypeDef *lptimHandle)
{

    if(lptimHandle->Instance == LPTIM1)
    {
        GPIO_InitTypeDef     GPIO_InitStruct;
        /* ## - 1 - Enable LPTIM clock ############################################ */
        __HAL_RCC_LPTIM1_CLK_ENABLE();
        /* ## - 2 - Force & Release the LPTIM Periheral Clock Reset ############### */
        /* Force the LPTIM Periheral Clock Reset */
        __HAL_RCC_LPTIM1_FORCE_RESET();
        /* Release the LPTIM Periheral Clock Reset */
        __HAL_RCC_LPTIM1_RELEASE_RESET();

        /* ## - 3 - Enable & Configure LPTIM Output ############################### */
        /* Configure PC1 (LPTIM1_OUT) in alternate function (AF0), Low speed
        push-pull mode and pull-up enabled.
        Note: In order to reduce power consumption: GPIO Speed is configured in
        LowSpeed */

        /* Enable GPIO PORT C */
        __HAL_RCC_GPIOC_CLK_ENABLE();

        /* Configure PC1 */
        GPIO_InitStruct.Pin = GPIO_PIN_1;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_PULLUP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW     ;
        GPIO_InitStruct.Alternate = GPIO_AF0_LPTIM1;
        HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

        HAL_NVIC_SetPriority(LPTIM1_IRQn, 1, 0);
        HAL_NVIC_EnableIRQ(LPTIM1_IRQn);
    }
}

void HAL_LPTIM_MspDeInit(LPTIM_HandleTypeDef *lptimHandle)
{

    if(lptimHandle->Instance == LPTIM1)
    {
        /* USER CODE BEGIN LPTIM1_MspDeInit 0 */

        /* USER CODE END LPTIM1_MspDeInit 0 */
        /* Peripheral clock disable */
        __HAL_RCC_LPTIM1_CLK_DISABLE();

        /* LPTIM1 interrupt Deinit */
        HAL_NVIC_DisableIRQ(LPTIM1_IRQn);
        /* USER CODE BEGIN LPTIM1_MspDeInit 1 */

        /* USER CODE END LPTIM1_MspDeInit 1 */
    }
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
