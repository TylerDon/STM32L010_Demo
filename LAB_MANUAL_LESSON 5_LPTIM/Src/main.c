/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "lptim.h"
#include "usart.h"
#include "gpio.h"
#define PWM_MODE 0
#define TIMEOUT_MODE 1

void SystemClock_Config(void);
static void LSI_ClockEnable(void);

extern LPTIM_HandleTypeDef hlptim1;

typedef struct
{
    uint16_t Counter;
    uint16_t AutoReload;
    uint16_t Compare;
}LpTIM_Config;
LpTIM_Config S_LpTIM_Config;

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
    HAL_Init();
    SystemClock_Config();
	  //LSI_ClockEnable();
    MX_GPIO_Init();//设置GPIO外设
    MX_LPTIM1_Init();//设置LPTIM外设
    MX_USART2_UART_Init();//配置USART外设
    /* USER CODE BEGIN 2 */
    printf("//*************************************// \r\n");
    printf("This is Test For LAB MANUAL LESSON 5\r\n");
    printf("LPTIM example\r\n");
#if PWM_MODE
    HAL_LPTIM_PWM_Start_IT(&hlptim1,//tim句柄
                           1999,//自动重装载值
                           999);//比较值
#elif TIMEOUT_MODE
	  /**
  * @brief  设置TimeOut中断，任意的一个翻转事件会重启计数器
  * @param  LPTIME句柄
  * @param  自动重装载值：当计数器Count累加到重装载值之后为0，自动重新装载（最大0Xffff）         
  * @param  Timeout计数器：当计数器累加到此值时，重启计数器
	*					小于自动重装载值，小于0XFFFF
  * @retval  函数的成功与失败
 	* @callback HAL_LPTIM_CompareMatchCallback
	* @example  假设LPtim的时钟是HSI（16M），LPTIM进行128分频，那么FCount = 0.125Mhz = 125khz	
  *           当 T =50000/125kHz =  之后重新加载;当T= 12500 /125kHz之后，唤醒Stop模式,如果T小于timeout时间，不会唤醒MCU
	*/
    HAL_LPTIM_TimeOut_Start_IT(&hlptim1, 
                               50000-1,//0.4s 
                               12500-1);//0.1s
#else





#endif
    while (1)
    {
        /* USER CODE END WHILE */

        /* USER CODE BEGIN 3 */
        //HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
        //HAL_Delay(50);
        //HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
        HAL_Delay(5);


    }
    /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
    RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

    /** Configure the main internal regulator output voltage
    */
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
    /** Initializes the CPU, AHB and APB busses clocks
    */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLLMUL_3;
    RCC_OscInitStruct.PLL.PLLDIV = RCC_PLLDIV_2;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }
    /** Initializes the CPU, AHB and APB busses clocks
    */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
                                  | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
    {
        Error_Handler();
    }
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2 | RCC_PERIPHCLK_LPTIM1;
    PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
    PeriphClkInit.LptimClockSelection = RCC_LPTIM1CLKSOURCE_HSI;//设置LPTIM输入时钟源为HSI 16Mhz
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
    {
        Error_Handler();
    }
}

/* USER CODE BEGIN 4 */
/*        */

static void LSI_ClockEnable(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable LSI clock */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
      Error_Handler();
  }
}

void HAL_LPTIM_CompareMatchCallback(LPTIM_HandleTypeDef *hlptim)
{
    printf("HAL_LPTIM_CompareMatchCallback\r\n");
		S_LpTIM_Config.Counter =  HAL_LPTIM_ReadCounter(&hlptim1);
		S_LpTIM_Config.AutoReload =  HAL_LPTIM_ReadAutoReload(&hlptim1);
		S_LpTIM_Config.Compare = HAL_LPTIM_ReadCompare(&hlptim1);
    HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
}

void HAL_LPTIM_CompareWriteCallback(LPTIM_HandleTypeDef *hlptim)
{
    printf("HAL_LPTIM_CompareWriteCallback\r\n");
}


void HAL_LPTIM_AutoReloadMatchCallback(LPTIM_HandleTypeDef *hlptim)
{

    printf("HAL_LPTIM_AutoReloadMatchCallback\r\n");
}
void HAL_LPTIM_TriggerCallback(LPTIM_HandleTypeDef *hlptim)
{
    printf("HAL_LPTIM_TriggerCallback\r\n");

}
void HAL_LPTIM_AutoReloadWriteCallback(LPTIM_HandleTypeDef *hlptim)
{
    printf("HAL_LPTIM_AutoReloadWriteCallback\r\n");


}
void HAL_LPTIM_DirectionUpCallback(LPTIM_HandleTypeDef *hlptim)
{
    printf("HAL_LPTIM_DirectionUpCallback\r\n");

}
void HAL_LPTIM_DirectionDownCallback(LPTIM_HandleTypeDef *hlptim)
{
    printf("HAL_LPTIM_DirectionDownCallback\r\n");

}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */

    /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
       tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
